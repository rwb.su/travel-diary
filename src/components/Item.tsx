import React from "react";
import cn from "classnames";
import { Card, Icon } from "antd";
import { useWithStore } from "../store";

const Item: React.FC<{ index: number }> = (props) => {
  const { index } = props;
  const [{ point: { date, country, text }, edit }, dispatch] = useWithStore((state) => ({ point: state.points[index], edit: state.edit === index }));
  console.log('render' + index, edit);
  return (
    <Card
      className={cn("item", { edit })}
      type="inner"
      title={<>
        Post #{index} at {date} being in <span className="country-name">{country}
          <Icon className="flag" type="flag" theme="filled" /></span>
      </>}
      extra={
        <span className="actions">
          <a href="javascript:void(0)" onClick={() => { dispatch({ type: "EDIT", index }) }} className="remove">Edit</a>
          <a href="javascript:void(0)" onClick={() => { dispatch({ type: "DELETE", index }) }} className="remove">Remove</a>
        </span>
      }>
      {text}
    </Card>
  );

}

export default Item;