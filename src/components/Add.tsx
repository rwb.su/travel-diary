import React, { useState, FormEventHandler } from "react";
import axios from "axios";
import moment from 'moment';
import cn from 'classnames';
import { FormComponentProps } from "antd/lib/form";
import { Button, Empty, Form, AutoComplete, DatePicker, Input } from "antd";
import { useWithStore, useWithDispatch} from "../store";
import { format } from "util";
import { Point } from "../store/types";
import { useDebouncedCallback } from 'use-debounce';
import { interopDefault, debounce } from "../utils";
const { TextArea } = Input;



type Props = {
} & FormComponentProps;


function useAutocomleteCity(initialCities: string[]) {
  const [cities, setCities] = useState<string[]>(initialCities);

  const [onSearch] = useDebouncedCallback(
    async (value: string) => {
      console.log('start func', value);
      const response = await axios.get<string, any>('https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + encodeURIComponent(value));
      console.log(response);
      const inputCities = response.data.response.GeoObjectCollection.featureMember.map((obj: any) => obj.GeoObject.description || obj.GeoObject.name || 'unknown') as string[];
      console.log(inputCities);
      const cities = Array.from(new Set(inputCities));
      console.log(cities);
      setCities(cities);
    },
    1000
  );


  return { cities, onSearch };
}



const Add: React.FC<Props> = (props) => {

  const [{ point, edit }, dispatch] = useWithStore((state) => ({ point: state.edit !== null ? state.points[state.edit] : null, edit: state.edit }));
  console.log('render Add');

  const { cities, onSearch } = useAutocomleteCity(new Array<string>());

  const { getFieldDecorator, getFieldError, resetFields, setFieldsValue } = props.form;


  const fieldCountry = 'country';
  const fieldCountryError = getFieldError(fieldCountry);

  const fieldDate = 'date';
  const fieldDateError = getFieldError(fieldDate);

  const fieldText = 'text';
  const fieldTextError = getFieldError(fieldText);

  const onSubmit = (e: React.FormEvent) => {

    e.preventDefault();

    props.form.validateFields(async (err, values: any) => {
      if (!err) {
        const { country, date, text } = values;
        const newPoint: Point = {
          country,
          date,
          text
        }
        point ? 
        dispatch({ type: "SAVE", point: newPoint, index: edit as number }) : 
        dispatch({ type: "ADD", point: newPoint });
        resetFields([fieldText, fieldCountry, fieldDate]);
      }
    });


  }


  return (
    <Form className={cn('add', {edit: point})} layout="vertical" onSubmit={onSubmit}>

      <div className="row-date-country">
     
          <Form.Item
            validateStatus={fieldCountryError ? 'error' : ''}
            help={fieldCountryError || ''}
            label="Country"
          >
            {getFieldDecorator(fieldCountry, {
              rules: [{ required: true }],
              validateTrigger: ['onChange', 'onBlur'],
              initialValue: point && point.country
            })(
              <AutoComplete
                dataSource={cities}
                onSearch={onSearch} />
            )}
          </Form.Item>
  


          <Form.Item
            validateStatus={fieldDateError ? 'error' : ''}
            help={fieldDateError || ''}
            label="Date"
          >
            <>


              <DatePicker onChange={(date, dateString) => { setFieldsValue({ [fieldDate]: dateString }) }} value={point && point.date && interopDefault(moment)(point.date) || undefined}/>
              {getFieldDecorator(fieldDate, {
                rules: [{ required: true}],
                validateTrigger: ['onChange', 'onBlur'],
                initialValue: point && point.date
              })(
                <Input type="hidden" />
              )}
            </>

          </Form.Item>

      
      </div>

      <div className="row-text">
        <Form.Item
          validateStatus={fieldTextError ? 'error' : ''}
          help={fieldTextError || ''}
        >

          {getFieldDecorator(fieldText, {
            rules: [{ required: true }],
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: point && point.text
          })(

            <TextArea placeholder="Message" rows={6} />
          )}

        </Form.Item>

      </div>

      <div>
        <Form.Item className="page-editor-item-form-actions">
          <Button type="primary" htmlType="submit" >{point ? 'Save' : 'Add'}</Button>
        </Form.Item>
      </div>



    </Form>
  );

}


export default Form.create<Props>()(Add);

