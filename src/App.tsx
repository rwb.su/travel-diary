import React from 'react';
import 'antd/dist/antd.css';
import './App.scss';
import List from './components/List';
import Add from './components/Add';
import { Provider } from './store';

const App: React.FC = () => {
  return (
    <Provider>
      <List />
      <Add />
    </Provider>
  );
}

export default App;
