import React from "react";

export type Action =
  | { type: 'ADD', point: Point }
  | { type: 'DELETE', index: number }
  | { type: 'EDIT', index: number }
  | { type: 'SAVE', index: number, point: Point }


export type Point = {
  date: string,
  country: string,
  text: string
}

export type StoreState = {
  points: Point[],
  edit: number | null;
}