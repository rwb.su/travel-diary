import React, { Dispatch, useContext, useMemo } from "react";
import { StoreState, Action, Point } from "./types";
import reducer from "./reducer";
import initialState from "./state";


const KEY = 'TRAVEL_DIARY';

const StoreContext = React.createContext<StoreState>(initialState);
const DispatchContext = React.createContext<React.Dispatch<Action>>((act: Action) => console.warn(act));

function usePersistedState(state: StoreState): StoreState {
  const persistedContext = localStorage.getItem(KEY);
  return persistedContext ? JSON.parse(persistedContext) : state;
}

function usePersistedReducer() {
  const globalStore = usePersistedState(initialState);
  const [state, dispatch] = React.useReducer(reducer, globalStore)

  React.useEffect(() =>
    localStorage.setItem(KEY, JSON.stringify(state)),
    [state]
  );

  return { state, dispatch };
}


export function Provider ({ children } : React.Props<any>) {

  const { state, dispatch } = usePersistedReducer();

  return React.createElement(StoreContext.Provider, { value: state }, 
    React.createElement(DispatchContext.Provider, { value: dispatch }, children));
}

export function useWithStore<StateExtract extends object>(
  extractState: (state: StoreState) => StateExtract
) {

  const state = useContext(StoreContext);
  const dispatch = useContext(DispatchContext);
  const stateExtract = extractState(state);

  return useMemo(() => {
    console.log('useWithStore', stateExtract);
     return [stateExtract, dispatch] as [StateExtract, React.Dispatch<Action>]
  }, Object.values(stateExtract))

}

export function useWithDispatch() {
  const dispatch = useContext(DispatchContext);
  return dispatch
}
