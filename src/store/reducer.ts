import React from "react";
import { StoreState, Action } from "./types";


const reducer: React.Reducer<StoreState, Action> = (state, action) => {
  console.log('reducer', state);
  switch (action.type) {
    case "EDIT":
      return {
        ...state,
        edit: action.index,
      };
    case "ADD":
      return {
        ...state,
        points: [...state.points, action.point]
      };
    case "SAVE": {
      const points = state.points;
      points[action.index] = action.point;
      const edit = null;
      return {
        ...state,
        edit,
        points
      };
    }
    case "DELETE": {
      const edit = null;
      return {
        ...state,
        edit,
        points: state.points.filter((p, index) => index !== action.index)
      };
    }

    default:
      return state;
  }
}

export default reducer;
