import React, { useMemo } from "react";
import { useWithStore } from "../store";
import Item from "./Item";

const List: React.FC = () => {
  const [{points}, dispatch] = useWithStore(state => ({points: state.points}));
  console.log('points ' + points);
  return  (
    <div className="list">
      {points.map((t, index) => {
        console.log('rebder list item ' + index);
        return <Item key={index} index={index} />
      })}
    </div>
  )


}

export default List;